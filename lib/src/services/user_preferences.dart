import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instance = UserPreferences._ctor();
  factory UserPreferences() {
    return _instance;
  }

  UserPreferences._ctor();

  SharedPreferences _prefs;

  init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  get data {
    return _prefs.getString('data') ?? '';
  }

  set data(String value) {
    _prefs.setString('data', value);
  }


  get userOnboarded {
    return _prefs.getBool('userOnboarded') ?? false;
  }

  get userRegister {
    return _prefs.getBool('userRegister') ?? false;
  }



  Future setUserOnboarded(bool value) {
    _prefs.setBool('userOnboarded', value);
  }

  Future setUserRegister(bool value) {
    _prefs.setBool('userRegister', value);
  }


  get userId {
    return _prefs.getString('user_id') ?? '';
  }

  Future setUserId(String value) {
    return _prefs.setString('user_id', value);
  }

  Future setUserName(String value) {
    return _prefs.setString('user_name', value);
  }

  get GetUserName {
    return _prefs.getString('user_name') ?? '';
  }


  Future setUserEmail(String value) {
    return _prefs.setString('user_email', value);
  }

  get GetUserEmail{
    return _prefs.getString('user_email') ?? '';
  }

  Future SetMenu(String value) {
    return _prefs.setString('menu_', value);
  }

  get GetMenu {
    return _prefs.getString('menu_') ?? '';
  }

  get firebaseId {
    return _prefs.getString('firebase_id') ?? '';
  }

  Future setFirebaseId(String value) {
    return _prefs.setString('firebase_id', value);
  }

  get motusToken {
    return _prefs.getString('motus_token') ?? '';
  }

  Future setMotusToken(String value) {
    _prefs.setString('motus_token', value);
  }

  get RiskColor {
    return _prefs.getString('motus_color') ?? '';
  }

  Future setRiskColor(String value) {
    _prefs.setString('motus_color', value);
  }
  get RiskDay {
    return _prefs.getString('motus_day') ?? '';
  }

  Future setRiskDay(String value) {
    _prefs.setString('motus_day', value);
  }
}
