import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dashboard/src/providers/UserProvider.dart';
import 'package:dashboard/src/services/user_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method
import 'dart:math'; // for the utf8.encode method
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);
  Future<String> signUp(String email, String password, String name);
  Future<void> sendEmailVerification();
  Future<void> signOut();
  Future<bool> isEmailVerified();
  Future<void> sendPasswordResetEmail({String email});
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String> signIn(String email, String password) async {

    var result = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    var user = result.user;
    UserPreferences().setFirebaseId(user.uid);
    UserPreferences().setUserId(user.uid);
    UserPreferences().setUserOnboarded(true);
    UserPreferences().setUserRegister(true);
    UserPreferences().setUserEmail(user.email.toString());
    UserPreferences().setUserName(user.displayName.toString());
    return user.uid;
  }

  Future<String> signUp(String email, String password, String name) async {
    var result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);

    result.user.updateDisplayName(name);
    var user = result.user;

    final bool UserRegistered = UserPreferences().userRegister;
    print(UserRegistered);

    if (UserRegistered) {
      UserPreferences().setFirebaseId(user.uid);
      UserPreferences().setUserId(user.uid);
      UserPreferences().setUserOnboarded(true);
    } else {
      UserPreferences().setFirebaseId(user.uid);
      UserPreferences().setUserOnboarded(true);
      UserPreferences().setUserId(user.uid);
    }
    return user.uid;
  }

  Future<String> getCurrentUser() async {
    var user = await _firebaseAuth.currentUser.displayName;
    return user;
  }


  Future<void> signOut() async {
    print('close all');
    UserPreferences().setUserId('');
    UserPreferences().setFirebaseId('');
    UserPreferences().setMotusToken('');
    UserPreferences().setUserOnboarded(false);
    UserPreferences().setUserRegister(false);

    _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    await _firebaseAuth.currentUser.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    var user = await _firebaseAuth.currentUser.emailVerified;
    return user;
  }

  @override
  Future<void> sendPasswordResetEmail({String email}) async {
    await _firebaseAuth.sendPasswordResetEmail(email: email);
  }
}
