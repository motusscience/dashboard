import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:dashboard/src/config/theming.dart';
import 'package:dashboard/src/services/authentication.dart';
import 'package:dashboard/src/services/user_preferences.dart';

class UserProvider with ChangeNotifier {
  UserPreferences _preferences;
  BaseAuth _auth;

  BaseAuth get auth => _auth;
  String _username;

  String get username => _username;
  String _user_id;

  String get user_id => _user_id;
  bool _userOnboarded;

  bool get userOnboarded => _userOnboarded;

  bool _userRegister;
  bool get userRegister => _userRegister;


  String _personalRiskLevel;

  String get personalRiskLevel => _personalRiskLevel;

  //Morris
  String _riskProfile;
  String get riskProfile => _riskProfile;

  String _blob;
  String get blob => _blob;

  String _blobColor;
  String get blobColor => _blobColor;

  void initAuth() {
    _auth = new Auth();
    notifyListeners();
  }

  void setUsername(String name) {
    _username = name;
    notifyListeners();
  }

  void setUserId(String userId) {
    _user_id = userId;
    notifyListeners();
  }

  void setPersonalRiskLevel(String riskLevel) {
    _personalRiskLevel = riskLevel;
    notifyListeners();
  }

  void setRiskColor(String blobColor) {
    _blobColor = blobColor;
    notifyListeners();
  }




  UserProvider();
}
