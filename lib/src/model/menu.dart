import 'package:flutter/material.dart';

class Menu {
  String title;
  IconData icon;
  String url;
  Menu({this.title, this.icon, this.url});
}

List<Menu> menuItems = [
  Menu(title: 'Tab1', icon: Icons.dashboard,url : 'tab1'),
  Menu(title: 'Tab2', icon: Icons.notification_important,url:'tab2'),
  Menu(title: 'Tab3', icon: Icons.web, url:'tab3'),
];
