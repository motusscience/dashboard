import 'dart:ui';

const Color brandNavy = Color(0xff333841);
const Color highRisk = Color(0xffFE7879);
const Color moderateRisk = Color(0xffFFC42E);
const Color lowRisk = Color(0xff80BD79);
const Color highRiskLight = Color(0xffFCE4EE);
const Color moderateRiskLight = Color(0xffFFF4DF);
const Color lowRiskLight = Color(0xffE0ECE3);
const Color noneRiskLight = Color(0xff333841);
const Color noneRisk= Color(0xffcbcbcb);