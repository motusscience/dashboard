import 'dart:io';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:dashboard/src/config/theming.dart';
import 'package:dashboard/src/services/authentication.dart';
import 'package:dashboard/src/services/user_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = new GlobalKey<FormState>();
  BaseAuth _auth;
  String _email;
  String _password;
  String _errorMessage;

  bool _isLoginForm;
  bool _isLoading;




  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {

    // Clean data
    //await _auth.signOut();


    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String userId = "";
      try {
        userId = await _auth.signIn(_email, _password);
        print('Signed in: $userId');

        setState(() {
          _isLoading = false;
        });

        Navigator.of(context).pushNamed('/test');

      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    } else {
      setState(() {
        _isLoading = false;
        _errorMessage = 'Could you please fill out the below info.';
        _formKey.currentState.reset();
      });
    }
  }

  @override
  void initState() {
    _auth = new Auth();
    _errorMessage = "";
    _isLoading = false;
    _isLoginForm = true;
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }

  @override
  Widget build(BuildContext context) {

    final _media = MediaQuery.of(context).size;

    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth <= 800) {
            return new Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: brandNavy,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: brandNavy,
                  elevation: 0,
                ),
                body: SingleChildScrollView(
                    child: Center(child: Container(width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.9, decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12.withOpacity(0.4),
                    ), child: Stack(
                      children: <Widget>[
                        _showForm(),
                        _showCircularProgress(),
                      ],
                    ),),)));
          } else {
            return new Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: brandNavy,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: brandNavy,
                  elevation: 0,
                ),
                body: SingleChildScrollView(
                    child: Center(child: Container(width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.35, decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12.withOpacity(0.4),
                    ), child: Stack(
                      children: <Widget>[
                        _showForm()
                      ],
                    ),),)));
          }
          });
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Icon(
            Icons.map,
            color: Colors.red,
          ),
        ),
      ),
    );
  }

  Widget _showForm() {
    return new Container(
        padding: EdgeInsets.only(top: 30.0, left: 30, right: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(
                    color: Colors.transparent,
                  )),
            ),
            new Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Image(image: AssetImage('assets/images/logo.png'), width: MediaQuery.of(context).size.width * 0.15,
                      height: MediaQuery.of(context).size.height * 0.1,),
                      SizedBox(height: 30),
                      // Text(
                      //   'Login',
                      //   style: TextStyle(
                      //       fontSize: 32, height: 1.2, color: Colors.white),
                      // ),
                      Center(child: Text(
                        'Login',
                        style: TextStyle(
                            fontSize: 24, height: 1.2, color: Colors.white),
                      )),
                      showErrorMessage(),
                      showEmailInput(),
                      showPasswordInput(),
                      SizedBox(height: 10),
                      _showCircularProgress(),
                      showPrimaryButton(),
                      SizedBox(height: 20),
                      showForgotPassword(),
                      SizedBox(height: 20),
                      showSecondaryButton(),
                      SizedBox(height: 30),
                      showSkipButton()
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        style: TextStyle(color: Colors.white),
        decoration: _inputDecoration('Email'),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        style: TextStyle(color: Colors.white),
        decoration: _inputDecoration('Password'),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showPrimaryButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: FlatButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.white,
            child: Text('Login',
                style: TextStyle(fontSize: 16.0, color: brandNavy)),
            onPressed: validateAndSubmit,
          ),
        ));
  }

  Widget showSecondaryButton() {
    return new FlatButton(
        child: Text('Create an account',
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w300)),
        onPressed: () async {
          print(UserPreferences().userRegister);
          Navigator.of(context).pushNamed('/signup');
        });
  }

  Widget showForgotPassword() {
    return new FlatButton(
        child: Text('Forgot password?',
            style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.w400)),
        onPressed: () => Navigator.of(context).pushNamed('/forgot-password'));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          _errorMessage,
          style: TextStyle(
              fontSize: 13.0,
              color: Colors.red,
              height: 1.0,
              fontWeight: FontWeight.w300),
        ),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }

  // TODO: rework this ...
  Widget showSkipButton() {
    return FlatButton(
        onPressed: () =>
            _isLoginForm ? null : Navigator.of(context).pushNamed('/sucess'),
        child: Text(
          _isLoginForm ? '' : 'Skip login',
          style: TextStyle(color: Colors.white),
        ));
  }

  InputDecoration _inputDecoration(String _hintText) {
    return new InputDecoration(
      hintText: _hintText,
      filled: true,
      hintStyle: TextStyle(color: Colors.white.withOpacity(.5)),
      fillColor: Colors.white.withOpacity(.1),
      contentPadding: const EdgeInsets.all(15),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white.withOpacity(0.1)),
        borderRadius: BorderRadius.circular(10),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
