import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dashboard/src/config/theming.dart';

class EmailVerification extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _media = MediaQuery.of(context).size;


    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth <= 800) {
            return new Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: brandNavy,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: brandNavy,
                  elevation: 0,
                ),
                body: SingleChildScrollView(
                    child: Center(child: Container(width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.9, decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12.withOpacity(0.4),
                    ), child: Wrap(
                    children: <Widget>[
                      SizedBox(height: 60),
                      Center(child: Image(image: AssetImage('assets/images/logo.png'), width: MediaQuery.of(context).size.width * 0.15,
                        height: MediaQuery.of(context).size.height * 0.1,)),
                      SizedBox(height: 100),
                      Center(child: _buildText()),
                        Center(child: _buildContinueButton(context)),Center(child: _buildTooltip(context)),
                      ],
                    ),),)));
          } else {
            return new Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: brandNavy,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: brandNavy,
                  elevation: 0,
                ),
                body: SingleChildScrollView(
                    child: Center(child: Container(width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.35, decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12.withOpacity(0.4),
                    ), child: Wrap(
                      children: <Widget>[
                        SizedBox(height: 60),
                        Center(child: Image(image: AssetImage('assets/images/logo.png'), width: MediaQuery.of(context).size.width * 0.15,
                          height: MediaQuery.of(context).size.height * 0.1,)),
                        SizedBox(height: 100),
                        Center(child: _buildText()),
                        Center(child:  _buildContinueButton(context)),Center(child: _buildTooltip(context))
                      ],
                    ),),)));
          }
        });

  }

  Widget _buildTooltip(context) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width * 0.3,
      child: Text(
        'Secure your personal data so you can access your account from any device by creating a account.',
        style: TextStyle(
          color: Colors.white,
          fontSize: 16,
          height: 1.5,
        ),
      ),
      padding: const EdgeInsets.all(20),
      margin: const EdgeInsets.only(top: 30, bottom: 70),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(.1),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(
            color: Colors.white.withOpacity(.1),
          )),
    );
  }

  Widget _buildText() {
    return Padding(
      padding: const EdgeInsets.only(left:40,right:40),
      child: Text(
          'We have sent you an email. Click on the link to verify your account.',
          style: TextStyle(
            color: Colors.white,
            fontSize: 24,
            height: 1.2,
          )),
    );
  }

  Widget _buildContinueButton(context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: FlatButton(
            minWidth: MediaQuery
                .of(context)
                .size
                .width * 0.3,
            // elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.white,
            child: Text('Continue ',
                style: TextStyle(fontSize: 16.0, color: brandNavy)),
            onPressed: () async {
        Navigator.of(context).pushNamed('/');
        },
          ),
        ));
  }
}
