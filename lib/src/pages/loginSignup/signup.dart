import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dashboard/src/config/theming.dart';
import 'package:dashboard/src/services/authentication.dart';
import 'package:dashboard/src/services/user_preferences.dart';



class SignupPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  BaseAuth _auth;

  final _formKey = new GlobalKey<FormState>();

  String _name;
  String _email;
  String _password;
  String _errorMessage;

  bool _isLoading;

  @override
  void initState() {
    _auth = new Auth();
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String userId = "";
      try {
        userId = await _auth.signUp(_email, _password, _name);
        _auth.sendEmailVerification();
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pushNamed('/email-verification');
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    } else {
      setState(() {
        _isLoading = false;
        _formKey.currentState.reset();
        _errorMessage = 'Could you please fill out the below info';
      });
    }
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }


  @override
  Widget build(BuildContext context) {

    final _media = MediaQuery.of(context).size;


    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth <= 800) {
            return new Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: brandNavy,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: brandNavy,
                  elevation: 0,
                ),
                body: SingleChildScrollView(
                    child: Center(child: Container(width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.9, decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12.withOpacity(0.4),
                    ), child: Stack(
                      children: <Widget>[
                        _showForm(),
                      ],
                    ),),)));
          } else {
            return new Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: brandNavy,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: brandNavy,
                  elevation: 0,
                ),
                body: SingleChildScrollView(
                    child: Center(child: Container(width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.35, decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12.withOpacity(0.4),
                    ), child: Stack(
                      children: <Widget>[
                        _showForm(),
                      ],
                    ),),)));
          }
        });
  }



  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Icon(
            Icons.map,
            color: Colors.red,
          ),
        ),
      ),
    );
  }

  Widget _showForm() {
    return Container(
        padding: EdgeInsets.all(30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: const EdgeInsets.all(20),
              margin: const EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(
                    color: Colors.transparent,
                  )),
            ),
            new Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Image(image: AssetImage('assets/images/logo.png'), width: MediaQuery.of(context).size.width * 0.15,
                        height: MediaQuery.of(context).size.height * 0.1,),
                      SizedBox(height: 30),
                      Center(child: Text(
                        'Create your account',
                        style: TextStyle(
                            fontSize: 24, height: 1.2, color: Colors.white),
                      )),
                      showErrorMessage(),
                      showNameInput(),
                      showEmailInput(),
                      showPasswordInput(),
                      SizedBox(height: 20),
                      showPrimaryButton(),
                      SizedBox(height: 20),
                      showSecondaryButton(),
                      _showCircularProgress()
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        style: TextStyle(color: Colors.white),
        decoration: _inputDecoration('Name'),
        validator: (value) => value.isEmpty ? 'Name can\'t be empty' : null,
        onSaved: (value) => _name = value.trim(),
      ),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        style: TextStyle(color: Colors.white),
        decoration: _inputDecoration('Email'),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        style: TextStyle(color: Colors.white),
        decoration: _inputDecoration('Password'),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showPrimaryButton() {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: FlatButton(
            // elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.white,
            child: Text('Create account',
                style: TextStyle(fontSize: 16.0, color: brandNavy)),
            onPressed: validateAndSubmit,
          ),
        ));
  }

  Widget showSecondaryButton() {
    return FlatButton(
        child: Text('Have an account? Sign in',
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w300)),
        onPressed: () => Navigator.of(context).pushNamed('/login'));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          _errorMessage,
          style: TextStyle(
              fontSize: 13.0,
              color: Colors.red,
              height: 1.0,
              fontWeight: FontWeight.w300),
        ),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }


  InputDecoration _inputDecoration(String _hintText) {
    return InputDecoration(
      hintText: _hintText,
      filled: true,
      hintStyle: TextStyle(color: Colors.white.withOpacity(.5)),
      fillColor: Colors.white.withOpacity(.1),
      contentPadding: const EdgeInsets.all(15),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white.withOpacity(0.1)),
        borderRadius: BorderRadius.circular(10),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
