import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dashboard/src/commons/theme.dart';
import 'package:dashboard/src/widget/card_tile.dart';
import 'package:dashboard/src/widget/chart_card_tile.dart';
import 'package:dashboard/src/widget/comment_widget.dart';
import 'package:dashboard/src/widget/profile_Widget.dart';
import 'package:dashboard/src/widget/project_widget.dart';
import 'package:dashboard/src/widget/quick_contact.dart';
import 'package:dashboard/src/widget/responsive_widget.dart';
import 'package:dashboard/src/pages/loginSignup/forgotPassword.dart';
import 'package:reactive_state/reactive_state.dart';
import 'package:dashboard/src/model/menu.dart';
import 'package:dashboard/src/widget/menu_item_tile.dart';
import 'package:dashboard/src/services/authentication.dart';
import 'package:dashboard/src/services/user_preferences.dart';
import 'package:dashboard/src/pages/tab1.dart';
import 'package:dashboard/src/pages/tab2.dart';
import 'package:dashboard/src/pages/tab3.dart';
import 'package:dashboard/src/providers/UserProvider.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage>
    with SingleTickerProviderStateMixin{

  double maxWidth = 250;
  double minWidgth = 70;
  double animation_weight = 250;
  bool collapsed = false;
  int selectedIndex = 0;
  String Select_tab = 'tab1';

  BaseAuth _auth;
  AnimationController _animationController;
  Animation<double> _animation;


  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween<double>(begin: maxWidth, end: minWidgth)
        .animate(_animationController);
    _auth = Auth();
  }

  @override
  Widget build(BuildContext context) {

    // Sign out
    void signOut() async {
      try {
        UserPreferences().setUserOnboarded(false);
        await _auth.signOut();
        Navigator.of(context).pushNamed('/login');

      } catch (e) {
        print('Error in signout: $e');
      }
    }



    // Menu
    Widget LeftMenu(context) {
      return AnimatedBuilder(
        animation: _animation,
        builder: (BuildContext context, Widget child) {
          return Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(blurRadius: 10, color: Colors.black26, spreadRadius: 2)
              ],
              color: Colors.white,
            ),
            width: _animation.value,
            child:  Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height:  80,
                  decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      image: DecorationImage(
                        image: AssetImage('assets/images/bg.jpg'),
                        fit: BoxFit.cover,
                      )),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    height: 100,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage:  AssetImage('assets/images/profile.jpg'),
                              backgroundColor: Colors.white,
                              radius: _animation.value >= animation_weight ? 30 : 20,
                            ),
                            SizedBox(
                              width: _animation.value >= animation_weight ? 20 : 0,
                            ),

                            Column(children: [(_animation.value >= animation_weight)
                                ? Text(
                              UserPreferences().GetUserName,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                                : Container(),
                              (_animation.value >= animation_weight)
                                  ? TextButton(
                                style: ButtonStyle(
                                  overlayColor: MaterialStateProperty.resolveWith<Color>(
                                          (Set<MaterialState> states) {
                                        if (states.contains(MaterialState.focused))
                                          return Colors.red;
                                        return Colors.transparent.withOpacity(0.3); // Defer to the widget's default.
                                      }
                                  ),
                                ),
                                onPressed: ()  async {
                                  await signOut();

                                },
                                child: Text('Sign out',
                                  style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),),
                              )
                                  : Container()],)

                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView.separated(
                    separatorBuilder: (context, counter) {
                      return Divider(
                        height: 2,
                      );
                    },
                    itemCount: menuItems.length,
                    itemBuilder: (BuildContext context, int index) {
                      return MenuItemTile(
                        title: menuItems[index].title,
                        icon: menuItems[index].icon,
                        animationController: _animationController,
                        isSelected: selectedIndex == index,
                        onTap: () {
                          setState(() {
                            selectedIndex = index;
                            Select_tab = menuItems[index].url;
                          });
                        },
                      );


                    },
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      collapsed = !collapsed;
                      collapsed
                          ? _animationController.reverse()
                          : _animationController.forward();
                    });
                  },
                  child: AnimatedIcon(
                    icon: AnimatedIcons.close_menu,
                    progress: _animationController,
                    color: Colors.black87,
                    size: 40,
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          );
        },
      );
    }


    // Menu
    Widget RouteMenu(String tab) {
      print(tab);
      if (tab=='tab1') {
        return(Tab1());
      } else if (tab=='tab2') {
        return(Tab2());
      } else if (tab=='tab3') {
        return(Tab3());
      } else {
        return(Container());
      }
    }

    final _media = MediaQuery.of(context).size;
    print(_media);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {

    // Case size // 1200
      return Material(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ResponsiveWidget.isLargeScreen(context)
                ? LeftMenu(context)
                : Container(),
            Flexible(
              fit: FlexFit.loose,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(child: RouteMenu(Select_tab.toString())),
                ],
              ),
            ),
          ],
        ),
      );
      // Case size


        return Container();
      },
    );
  }
}
