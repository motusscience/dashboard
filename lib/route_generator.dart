import 'package:dashboard/src/services/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:dashboard/src/pages/main_page.dart';
import 'package:dashboard/src/pages/loginSignup/login.dart';
import 'package:dashboard/src/pages/loginSignup/EmailVerification.dart';
import 'package:dashboard/src/pages/loginSignup/signup.dart';
import 'package:dashboard/src/pages/loginSignup/forgotPassword.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) {
          // IF USER HAS BEEN ONBOARDED AND IF USER ID HAS BEEN SET
          if (UserPreferences().userOnboarded &&
              UserPreferences().userId != null && UserPreferences().userId == '') {
            // IF PERMISSIONS HAVE BEEN SET
            return MainPage();
          } else {
            return LoginPage();
            //return MainPage();

          }
        });
        break;
      case '/home':
        return MaterialPageRoute(builder: (_) => MainPage());
        break;
      case '/forgot-password':
        return MaterialPageRoute(builder: (_) => ForgotPasswordPage());
        break;
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginPage());
        break;
      case '/signup':
        return MaterialPageRoute(builder: (_) => SignupPage());
        break;
      case '/email-verification':
        return MaterialPageRoute(builder: (_) => EmailVerification());
        break;

      default:
        return MaterialPageRoute(builder: (_) => MainPage());
        break;
    }
  }
}




